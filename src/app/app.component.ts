import { Component } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';

import { of } from 'rxjs/observable/of';
import { _throw } from 'rxjs/observable/throw';

import { catchError, tap, map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-root',
  template: `
    {{title}}
<pre>
{{ contacts$ | async | json }}
</pre>
<button (click)="getContacts()">Get Contacts</button>

    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'app';
  contacts$: Observable<any>;

  private apiUrl = 'api/contacts';
  constructor(private http: HttpClient) { }
  getContacts() {
    this.contacts$ = this.http.get<any>(this.apiUrl);
  }
}
