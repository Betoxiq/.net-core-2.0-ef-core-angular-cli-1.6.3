using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dnt.Database;
using dnt.Models;
using Microsoft.AspNetCore.Mvc;
namespace dnt.Controllers {
  [Route ("api/contacts")]
  public class ContactsController : Controller {
    private ContactsDbContext _dbContext;

    public ContactsController (ContactsDbContext dbContext) {
      this._dbContext = dbContext;
    }

    [HttpGet ()]
    public List<Contact> Get () {
      return this._dbContext.Contacts.ToList ();
    }

    [HttpGet ("{id}")]
    public Contact Get (int id) {
      return this._dbContext.Contacts.FirstOrDefault (e => e.Id == id);
    }

    [HttpPost]
    public IActionResult Post ([FromBody] Contact contact) {
      if (contact == null) {
        return BadRequest ();
      }

      var p = _dbContext.Contacts.FirstOrDefault (t => t.Id == contact.Id);
      if (p != null) {
        return BadRequest ();
      }

      this._dbContext.Contacts.Add (contact);
      this._dbContext.SaveChanges ();

      return Created ("Get", contact);
    }

    [HttpPut ("{id}")]
    public IActionResult Put (int id, [FromBody] Contact contactIn) {
      if (contactIn == null || contactIn.Id != id) {
        return BadRequest ();
      }

      var contact = _dbContext.Contacts.FirstOrDefault (t => t.Id == id);
      if (contact == null) {
        return NotFound ();
      }

      contact.Name = contactIn.Name;

      _dbContext.Contacts.Update (contact);
      _dbContext.SaveChanges ();
      return new OkObjectResult (contact);
    }

    [HttpDelete ("{id}")]
    public IActionResult Delete (int id) {
      var todo = _dbContext.Contacts.FirstOrDefault (t => t.Id == id);
      if (todo == null) {
        return NotFound ();
      }

      _dbContext.Contacts.Remove (todo);
      _dbContext.SaveChanges ();
      return new NoContentResult ();
    }
  }
}
