﻿using dnt.Database;
using dnt.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace dnt {
  public class Startup {
    public Startup (IConfiguration configuration) {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices (IServiceCollection services) {
      services.AddMvc (options => {
          options.FormatterMappings.SetMediaTypeMappingForFormat ("xml", "application/xml");
          options.FormatterMappings.SetMediaTypeMappingForFormat ("json", "application/json");
        })
        .AddXmlSerializerFormatters ();

      // Adds a default in-memory implementation of IDistributedCache.
      services.AddDistributedMemoryCache ();
      services.AddDbContext<ContactsDbContext> (options => options.UseInMemoryDatabase ("Contacts"));

    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure (IApplicationBuilder app, IHostingEnvironment env) {
      if (env.IsDevelopment ()) {
        app.UseDeveloperExceptionPage ();
      }
      app.UseDefaultFiles ();
      app.UseStaticFiles ();
      app.UseMvc ();

      using (var serviceScope = app.ApplicationServices.CreateScope ()) {
        var context = serviceScope.ServiceProvider.GetService<ContactsDbContext> ();
        context.Contacts.Add (new Contact { Id = 1, Name = "Jane" });
        context.Contacts.Add (new Contact { Id = 2, Name = "Joe" });
        context.Contacts.Add (new Contact { Id = 3, Name = "Pat" });
        context.SaveChanges ();
      }
    }

  }
}
