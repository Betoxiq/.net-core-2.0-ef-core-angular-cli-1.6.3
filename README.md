
Working with Angular/Cli + ASP.NET Web API + VSCode !
===================


This sample is meant as starting steps to create a standard ASP.NET WEB API project using dotnet Cli command and integrate with Angualr library with the help of @Angular/Cli command to make use of all the cli features.

 **Step 0: Clone this repo:**
-----------------------------
- $ git clone https://Betoxiq@bitbucket.org/Betoxiq/.net-core-2.0-ef-core-angular-cli-1.6.3.git <your-local-foder-here>
- $ cd <your-local-foder-here>

 **Step 1: Install NodeJs:**
-----------------------------

 - Download and install node js from https://nodejs.org/en/download/

 **Step 2: Install @Angular/Cli:**
-----------------------------------

 - Download and install @Angular/Cli using  **npm install -g @angular/cli**

 **Step 3: Install Visual Studio Code:**
-----------------------------------------

- Download and install latest version of Visual Studio Code from https://code.visualstudio.com/docs/setup/windows

 **Step 4: dotnet SDK :**
-----------------------------------------

- Download and install latest version of dotnet core SDK from https://www.microsoft.com/net/download/

 **Step 5: build Angular app :**
-----------------------------------------
- $ npm i 
- $ ng build 
- this will build the Angular front end app and copy it to wwwroot folder

 **Step 6: Import dotnet dependencies
-----------------------------------------
- $ dotnet restore 

 **Step 7: Debug app
-----------------------------------------
- $ open debug tab on vscode (Ctrl+Shift+D), select ".Net+Browser" from Lauch options dropdown.
 Start debug session (F5). Enjoy!


