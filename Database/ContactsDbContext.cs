using dnt.Models;
using Microsoft.EntityFrameworkCore;
namespace dnt.Database {
  public class ContactsDbContext : DbContext {
    public ContactsDbContext (DbContextOptions<ContactsDbContext> options) : base (options) { }
    public DbSet<Contact> Contacts { get; set; }
  }
}
